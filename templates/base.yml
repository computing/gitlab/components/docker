# -- Common base elements for Docker CI/CD components

spec:
  inputs:
    job_prefix:
      default: "docker"
      description: "Prefix to give to all job templates"
    image_name:
      default: "$CI_REGISTRY_IMAGE"
      description: "The name of the image to deploy"
    image_tag:
      default: ""
      description: "The tag to deploy"
    deploy_when:
      default: tags
      description: "When to deploy images and downstream triggers"
      options:
        - tags
        - default
        - all
    runner_tags:
      default: [executor-docker]
      description: "GitLab runner tags to apply to jobs"
      type: array
    upstream_project_path:
      default: ""
      description: "Path of the project (to distinguish it from forks)"
      type: string

---

.$[[ inputs.job_prefix ]]_base:
  tags: $[[ inputs.runner_tags ]]
  variables:
    CI_APPLICATION_REPOSITORY: "$[[ inputs.image_name ]]"
    CI_APPLICATION_TAG: "$[[ inputs.image_tag ]]"
    REGISTRY: "$CI_REGISTRY"
    REGISTRY_USER: "gitlab-ci-token"
    REGISTRY_PASSWORD: "$CI_JOB_TOKEN"
  before_script:
    - docker login -u ${REGISTRY_USER} -p ${REGISTRY_PASSWORD} ${REGISTRY}
  after_script:
    - docker logout ${REGISTRY}

.$[[ inputs.job_prefix ]]_deploy_rules:
  rules:
    # don't run on pushes to forks
    - if: '"$[[ inputs.upstream_project_path | expand_vars ]]" && $CI_PROJECT_PATH != "$[[ inputs.upstream_project_path | expand_vars ]]"'
      when: never
    # deploy tags
    - if: '"$[[ inputs.deploy_when ]]" == "tags" && $CI_COMMIT_TAG'
    # deploy on pushes
    - if: '"$[[ inputs.deploy_when ]]" == "default" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    # run on pushes to all branches
    - if: '"$[[ inputs.deploy_when ]]" == "all" && $CI_COMMIT_BRANCH'
