# -- Push docker images to registries

spec:
  inputs:
    stage:
      default: deploy
      description: "Pipeline stage to add build job to"
      type: string
    image_name:
      default: "$CI_REGISTRY_IMAGE"
      description: "The name of the image to deploy"
    pull_image_tag:
      default: "$CI_COMMIT_SHA"
      description: "The image tag to pull"
    push_image_tag:
      default: "$CI_COMMIT_REF_SLUG"
      description: "The tag to push"
    upstream_project_path:
      type: string
      default: ""
      description: "Path of the project (to distinguish it from forks)"
    push_when:
      default: tags
      description: "When to push images to registries"
      options:
        - tags
        - default
        - all
    tag_latest:
      default: true
      description: "If true tag the new image as 'latest' and push to registries"
      type: boolean
    docker_io_repository:
      default: ""  # do not push to docker.io
      description: "Name of target repository on dockerhub"
      type: string
    quay_io_repository:
      default: ""  # do not push to quay.io
      description: "Name of target repository on quay.io"
      type: string

---

include:
  - local: templates/base.yml
    inputs:
      deploy_when: "$[[ inputs.push_when ]]"
      image_name: "$[[ inputs.image_name | expand_vars ]]"
      image_tag: "$[[ inputs.pull_image_tag | expand_vars ]]"
      upstream_project_path: "$[[ inputs.upstream_project_path | expand_vars ]]"

docker_push:
  extends: .docker_base
  stage: $[[ inputs.stage ]]
  rules:
    # this is a customisable template
    - when: never
  variables:
    # tag name or branch name
    PUSH_IMAGE_TAG: "$[[ inputs.push_image_tag | expand_vars ]]"
    PUSH_IMAGE: "$[[ inputs.image_name | expand_vars ]]:$PUSH_IMAGE_TAG"
  before_script:
    - !reference [.docker_base, before_script]
    # set the pull and push images
    - PULL_IMAGE="${PULL_IMAGE:-$CI_APPLICATION_REPOSITORY:$CI_APPLICATION_TAG}"
  script:
    # pull image from local registry
    - docker pull ${PULL_IMAGE}
    # prepend the registry to the push image if necessary
    - if [[ "${PUSH_IMAGE}" != "${REGISTRY}/"* ]]; then
        PUSH_IMAGE="${REGISTRY}/${PUSH_IMAGE}";
      fi
    - |
      # tag the image
      echo -e "\x1B[92m+ docker tag ${PULL_IMAGE} ${PUSH_IMAGE}\x1B[0m";
      docker tag ${PULL_IMAGE} ${PUSH_IMAGE}
    - |
      # push tagged image
      echo -e "\x1B[92m+ docker push ${PUSH_IMAGE}\x1B[0m";
      docker push ${PUSH_IMAGE}
    - |
      # tag and push 'latest' image, if asked
      if "$[[ inputs.tag_latest ]]" && [[ "$CI_COMMIT_TAG" || "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]] && [[ "${PUSH_IMAGE_TAG}" != "latest" ]]; then
        LATEST_IMAGE="${PUSH_IMAGE%%:*}:latest";
        echo -e "\x1B[92m+ docker tag ${PULL_IMAGE} ${LATEST_IMAGE}\x1B[0m";
        docker tag ${PULL_IMAGE} ${LATEST_IMAGE};
        echo -e "\x1B[92m+ docker push ${LATEST_IMAGE}\x1B[0m";
        docker push ${LATEST_IMAGE};
      fi

# extension of the customisable template, but with proper rules
.docker_push:
  extends:
    - docker_push
    - .docker_deploy_rules

docker_push_gitlab:
  extends: .docker_push
  variables:
    PUSH_IMAGE: "$CI_APPLICATION_REPOSITORY:$PUSH_IMAGE_TAG"

docker_push_docker_io:
  extends: .docker_push
  variables:
    PUSH_IMAGE: "$[[ inputs.docker_io_repository ]]:$PUSH_IMAGE_TAG"
    REGISTRY: "docker.io"
    REGISTRY_USER: "$DOCKER_HUB_USER"
    REGISTRY_PASSWORD: "$DOCKER_HUB_TOKEN"
  rules:
    - if: '"$[[ inputs.docker_io_repository ]]" == ""'
      when: never
    - !reference [.docker_deploy_rules, rules]

docker_push_quay_io:
  extends: .docker_push
  variables:
    PUSH_IMAGE: "$[[ inputs.quay_io_repository ]]:$PUSH_IMAGE_TAG"
    REGISTRY: "quay.io"
    REGISTRY_USER: "$QUAY_USER"
    REGISTRY_PASSWORD: "$QUAY_TOKEN"
  rules:
    - if: '"$[[ inputs.quay_io_repository ]]" == ""'
      when: never
    - !reference [.docker_deploy_rules, rules]
