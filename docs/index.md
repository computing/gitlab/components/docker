# Docker components

This `computing/gitlab/components/docker` project provides
[CI/CD components](https://git.ligo.org/help/ci/components/index.html)
to help configure a [GitLab CI/CD](https://git.ligo.org/help/ci/index.html) pipeline
to build, test, and deploy container images using [Docker](https://www.docker.com).

## Latest release

[![latest badge](https://git.ligo.org/computing/gitlab/components/docker/-/badges/release.svg?text=test)](https://git.ligo.org/explore/catalog/computing/gitlab/components/docker/ "See latest release in the CI/CD catalog")

## Components

The following components are available:

- [`docker/build`](./build.md "`docker/build` component documentation")
- [`docker/test`](./test.md "`docker/test` component documentation")
- [`docker/push`](./push.md "`docker/push` component documentation")
- [`docker/trigger`](./trigger.md "`docker/trigger` component documentation")

In addition the following meta-components (combinations) of the above components
are available:

- [`docker/all`](./all.md "`docker/all` component documentation")
