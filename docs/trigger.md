# `docker/trigger`

Configure jobs to trigger pipelines in downstream projects after a new docker
image has been built in _this_ project.

## Usage

```yaml
include:
  - component: git.ligo.org/computing/cicd-components/docker/trigger@<VERSION>
    inputs:
      stage: trigger
      trigger_projects:
        - group/downstream1
        - group/downstream2
```

## Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` | `trigger` | The pipeline stage to add jobs to. |
| `job_name` | `docker_trigger` | The name to give the trigger job |
| `trigger_branch` | `$CI_COMMIT_BRANCH` {: .nowrap } | The branch in the downstream project to trigger. |
| `trigger_projects` | | The list of project paths to trigger. |
| `trigger_when` | `"tags"` | When to trigger pipelines in downstream projects, one of `"tags"` (only for git tags), `"default"` (pushes to the default git branch), `"all"` (pushes to all git branches) |
| `upstream_project_path` {: .nowrap } | | Path of the upstream project (to ensure that `push` jobs don't run on pipelines for forks of the upstream project). |

## Notes

### Use this component multiple times to trigger different branches { #branches }

The component configures downstream pipelines for multiple projects with a
single git branch name.
To trigger downstream pipelines for multiple branch names (either in a single
project or multiple projects), `include` the component multiple times with a
unique value for the `job_name` input (to not override the trigger
configuration again and again).

See the [_Trigger a rebuild of a downstream image_](#trigger) example below
for an example.

## Examples

### Trigger a rebuild of a downstream image { #trigger }

!!! example "Trigger a rebuild of a downstream project image"

    This example triggers a new pipeline for the default branch of
    the `group/subgroup/project1` and `group/subgroup/project2` projects
    _and_ of the `fedora` branch of the `group/subgroup/project3` project,
    whenever a new image is built for a git tag of this project:

    ```yaml
    include:
      - component: git.ligo.org/computing/gitlab/components/docker/build@<VERSION>
      - component: git.ligo.org/computing/gitlab/components/docker/push@<VERSION>
      - component: git.ligo.org/computing/gitlab/components/docker/trigger@1
        inputs:
          trigger_projects:
            - group/subgroup/project1
            - group/subgroup/project2
      - component: git.ligo.org/computing/gitlab/components/docker/trigger@1
        inputs:
          job_name: docker_trigger_fedora
          trigger_branch: fedora
          trigger_projects:
            - group/subgroup/project3
    ```
