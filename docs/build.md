# `docker/build`

Configure jobs to build an image using Docker and then scan it for vulnerabilities.

## Usage

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/docker/build@<VERSION>
    inputs:
      stage: build
```

## Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage`              | `build` | The pipeline stage to add jobs to |
| `dockerfile`         | | The path of the `Dockerfile` to build |
| `default_image_name` {: .nowrap } | `<image_name>` | The name of the image to compare to when scanning a newly built image. Set to the name of the image in the upstream project to enable scanning from forks. |
| `image_name`         | `$CI_REGISTRY_IMAGE` {: .nowrap } | The name of the image to push to the container registry |
| `image_tag`          | | The tag to give this image in the registry, defaults to the commit SHA |
