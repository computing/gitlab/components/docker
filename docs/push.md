# `docker/push`

Configure jobs to push newly-built images from the GitLab Container Registry
to other registries, notably Docker Hub and Quay.io, or to apply
production-like tags to existing images on the GitLab Container Registry.

## Usage

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/docker/push@<VERSION>
    inputs:
      stage: publish
```

## Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` | `publish` | The pipeline stage to add jobs to |
| `image_name` | `$CI_REGISTRY_IMAGE` {: .nowrap } | The name of the image to push to the container registry. |
| `pull_image_tag` | | The image to pull, should match the `image_tag` passed to the `build` component |
| `push_image_tag` | | The new tag to apply and push, defaults to the git tag or branch name |
| `push_when` | `"tags"` | When to push tagged images to registries, one of `"tags"` (only for git tags), `"default"` (pushes to the default git branch), `"all"` (pushes to all git branches) |
| `tag_latest` | `true` | If `true` tag the new image as 'latest' and push to registries |
| `docker_io_repository` | | Name of target repository on Docker Hub |
| `quay_io_repository` | | Name of target repository on Quay.io |
| `upstream_project_path` {: .nowrap } | | Path of the upstream project (to ensure that `push` jobs don't run on pipelines for forks of the upstream project). |

Notes:

-   In all pipelines, this component creates a job called `docker_push_gitlab`
    that will push a tagged image to the local gitlab container regsitry.

-   With the `tag_latest` input, a `latest` image is only tagged for git tag or
    default branch pipelines.

-   For details on pushing to Docker Hub see [_Pushing to Docker Hub_](#dockerhub).

-   For details on pushing to Quay.io see [_Pushing to Quay.io_](#quay.io).

## Pushing to Docker Hub {: #dockerhub }

If the `docker_io_repository` input is specified, this component creates a job
called `docker_push_docker_io` that will push a tagged image to the designated
repository on the Docker Hub container registry.

This job requiress the following additional variables to be defined as
[masked, protected project variables](
https://git.ligo.org/help/ci/variables/index.html#for-a-project
):

| Name               | Purpose |
| ------------------ | ------- |
| `DOCKER_HUB_USER`  | The username to user when authenticating to <https://hub.docker.com> with `docker login` |
| `DOCKER_HUB_TOKEN` | The [access token](https://docs.docker.com/docker-hub/access-tokens/) to use when authenticating |

## Pushing to Quay.io {: #quay.io }

If the `quay_io_repository` input is specified, this component creates a job
called `docker_push_quay_io` that will push a tagged image to the designated
repository on the Quay.io container registry.

This job requiress the following additional variables to be defined as
[masked, protected project variables](
https://git.ligo.org/help/ci/variables/index.html#for-a-project
):

| Name         | Purpose |
| ------------ | ------- |
| `QUAY_USER`  | The username to user when authenticating to <https://quay.io> with `docker login` |
| `QUAY_TOKEN` | The robot account [password](https://docs.quay.io/glossary/robot-accounts.html) to use when authenticating |
