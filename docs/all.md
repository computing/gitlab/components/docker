# `docker/all`

Configure an end-to-end pipeline that includes jobs from each of the
[`build`](./build.md),
[`test`](./test.md),
[`push`](./push.md),
and [`trigger`](./trigger.md) components.

## Usage

```yaml
include:
  - component: git.ligo.org/computing/cicd-components/docker/trigger@<VERSION>
```

## Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `dockerfile` | | The path of the `Dockerfile` to build |
| `deploy_when` | `"tags"` | When to deploy images and trigger downstream projects; one of `"tags"` (only for git tags), `"default"` (pushes to the default git branch), `"all"` (pushes to all git branches) |
| `default_image_name` | `<image_name>` | The name of the image to compare to when scanning a newly built image. Set to the name of the image in the upstream project to enable scanning from forks. |
| `image_name` | `$CI_REGISTRY_IMAGE` {: .nowrap } | The name of the image to push to the container registry |
| `image_tag` | | The tag to give this image in the registry, defaults to the commit SHA |
| `upstream_project_path` {: .nowrap } | | Path of the upstream project (to ensure that `push` jobs don't run on pipelines for forks of the upstream project). |
| `test_script` | Required | Array of commands to run as the `script` section of the test job |
| `docker_io_repository` | | Name of repository on Docker Hub to push to |
| `quay_io_repository` | | Name of repository on Quay.io to push to |
| `trigger_projects` | | The list of downstream project paths to trigger. |

## Example

!!! example "`docker/all` example"

    ```yaml
    include:
      - component: git.ligo.org/computing/gitlab/components/docker/all@1
        inputs:
          # validate that the tool is functional
          test_script:
            - my_tool --help
          # only deploy from git tags
          deploy_when: tags
          # only deploy things from this project, not from forks
          upstream_project_path: "namespace/project"
          # deploy to
          docker_io_repository: "username/repository"
          quay_io_repository: "username/repository"
    ```

    The above configuration would result in the following pipeline:

    ```mermaid
    flowchart LR;
      subgraph build
        direction LR
        job_build[<code>build</code>]
      end
      subgraph test
        direction LR
        job_scan[<code>container_scanning</code>]
        job_test[<code>docker_test</code>]
      end
      subgraph deploy
        direction LR
        job_push_gitlab[<code>docker_push_gitlab</code>]
        job_push_docker[<code>docker_push_docker_io</code>]
        job_push_quay[<code>docker_push_quay_io</code>]
      end
      build --> test --> deploy
    ```
