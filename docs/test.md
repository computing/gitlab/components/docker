# `docker/test`

Configure jobs to test a newly built container image.

## Usage

```yaml
include:
  - component: git.ligo.org/computing/gitlab/components/docker/test@<VERSION>
    inputs:
      test_script:
        - <command1>
        - <command2>
```

## Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `job_name` | `docker_test` | The name to give the test job |
| `stage` | `build` | The pipeline stage to add jobs to |
| `image_name` | `$CI_REGISTRY_IMAGE` {: .nowrap } | The name of the image to test |
| `image_tag` | `$CI_COMMIT_SHA` | The tag of the image to test |
| `test_script` { .nowrap } | Required | Array of commands to run as the `script` section of the test job |

## Examples

### Test a container image for a Python package { #python }

!!! example "Test a container image for a python package"

    ```yaml
    include:
      # build the container image, and scan it for vulnerabilities
      - component: git.ligo.org/computing/gitlab/components/docker/build@<VERSION>
      # test the new container image
      - component: git.ligo.org/computing/gitlab/components/docker/test@<VERSION>
          test_script:
            - dnf -y install python3 python3-pytest
            - python3 -m pytest --pyargs my_package
    ```
