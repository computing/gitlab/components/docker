# CI/CD components for Docker

This project provides [CI/CD components](https://git.ligo.org/help/ci/components/index.html)
to help configure a [GitLab CI/CD](https://git.ligo.org/help/ci/index.html) pipeline
to build, test, and deploy container images using [Docker](https://www.docker.com).

[[_TOC_]]

## Components

The following components are fully documented as part of the
IGWN Computing Guide:

- [`docker/all`](https://computing.docs.ligo.org/guide/gitlab/components/docker/all/)
- [`docker/build`](https://computing.docs.ligo.org/guide/gitlab/components/docker/build/)
- [`docker/test`](https://computing.docs.ligo.org/guide/gitlab/components/docker/test/)
- [`docker/push`](https://computing.docs.ligo.org/guide/gitlab/components/docker/push/)
- [`docker/trigger`](https://computing.docs.ligo.org/guide/gitlab/components/docker/trigger/)

## Contributing

Please read about CI/CD components and best practices at: <https://git.ligo.org/help/ci/components/index.html>.

All interactions related to this project should follow the
[LIGO-Virgo-KAGRA Code of Conduct](https://dcc.ligo.org/LIGO-M1900037).

For more details on contributing to this project, see `CONTRIBUTING.md`.

## Releases

To create a new release of this component, go to
<https://git.ligo.org/computing/gitlab/components/docker/-/tags/new>
and create a new tag.
**You must use a semantic version for the tag name**, e.g. 1.2.3, without
any prefix or suffix.
Feel free to include a tag message, but this is not required.

The CI/CD pipeline triggered for the tag will then automatically create a new
[release](https://git.ligo.org/computing/gitlab/components/docker/-/releases)
which will be published to the
[CI/CD Catalog](https://git.ligo.org/explore/catalog/computing/gitlab/components/docker).
